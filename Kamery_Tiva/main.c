// Kontroler Kamer Bloków Diagnostycznych - TI TM4C123GH6PM
// Copyright (C) 2020 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "tm4c123gh6pm.h"
#include "params.h"
#include "mbreg.h"

int main(void)
{
    /*------------------------ Variables ------------------------------------------------*/

    UCHAR ucActiveCamera = 0;

    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------ GPIO Pin Configuration -----------------------------------*/

    //Enable GPIO peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

    // UART
    GPIOPinConfigure(GPIO_PC6_U3RX);
    GPIOPinConfigure(GPIO_PC7_U3TX);
    GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_6 | GPIO_PIN_7);

    // External LED
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5);

    //Camera number output
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6);

    /*------------------------------ MODBUS Initialization ----------------------------------------*/

    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    (void) eMBEnable();

    while (1)
    {
        /*--------------------------------- MODBUS ----------------------------------------------------*/

        (void) eMBPoll();

        /*--------------------------------- Camera Switching ------------------------------------------*/

        //New command received
        if (bMBHoldingRegisterIsWritten(1))
        {
            if (usMBHoldingReg[0] <= NUM_CAMERAS && usMBHoldingReg[0] != ucActiveCamera)
            {
                ucActiveCamera = usMBHoldingReg[0] & 0x1F;
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6, (ucActiveCamera ^ 0x0F) << 2);
            }
        }
    }
}
