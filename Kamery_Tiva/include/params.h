#ifndef INCLUDE_PARAMS_H_
#define INCLUDE_PARAMS_H_

/* ----------------------- Defines ------------------------------------------*/

#define MB_MODE              (MB_RTU)
#define MB_SLAVEID           (0x01)
#define MB_PORT              (0)
#define MB_BAUDRATE          (19200)
#define MB_PARITY            (MB_PAR_EVEN)

#define MOTOR_TIMEOUT        (10)
#define SENS_LIMIT           (256)
#define SENS_HYSTERESIS      (264)

#define NUM_CAMERAS          (18)

#endif /* INCLUDE_PARAMS_H_ */
