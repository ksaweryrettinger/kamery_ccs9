#ifndef MBPORT_INCLUDE_MBREG_H_
#define MBPORT_INCLUDE_MBREG_H_

#include <limits.h>

/* ----------------------- Defines ------------------------------------------*/

#define REG_COILS_START      (1)      //coils starting address
#define REG_COILS_NREGS      (1)      //number of coils

#define REG_DISCRETE_START   (10001)  //discretes starting addres
#define REG_DISCRETE_NREGS   (0)      //number of discrete inputs

#define REG_INPUT_START      (30001)  //input registers starting address
#define REG_INPUT_NREGS      (0)      //number of input registers

#define REG_HOLDING_START    (40001)  //holding registers starting address
#define REG_HOLDING_NREGS    (1)      //number of holding registers

#define REG_NUM_BYTES        (2)      //number of bytes in register
#define REG_ADDR_OFFSET      (1)      //bit/register address offset

/* ----------------------- Global variables ----------------------------------*/

UCHAR  ucMBCoils[(REG_COILS_NREGS + (CHAR_BIT - 1)) / CHAR_BIT];            //Coils
UCHAR  ucMBDiscretes[(REG_DISCRETE_NREGS + (CHAR_BIT - 1)) / CHAR_BIT];     //Discrete Inputs
USHORT usMBInputReg[REG_INPUT_NREGS];                                       //Input Registers
USHORT usMBHoldingReg[REG_HOLDING_NREGS];                                   //Holding Registers

BOOL bMBCoilIsWritten(UCHAR ucIndex);
BOOL bMBHoldingRegisterIsWritten(UCHAR ucIndex);

#endif /* MBPORT_INCLUDE_MBREG_H_ */
