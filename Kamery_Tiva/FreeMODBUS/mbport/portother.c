#include "port.h"

/* ----------------------- Other functions -----------------------------*/

void LEDStatus(BOOL bStatus)
{
    if (bStatus) GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5); //switch ON LED
    else GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0); //switch OFF LED
}
